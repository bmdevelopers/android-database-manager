package co.bytemark.android.opentools.databasemanager;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteException;
import android.database.sqlite.SQLiteOpenHelper;
import android.os.Handler;
import co.bytemark.android.opentools.delegatehelper.DelegateHelper;




/**
 * DatabaseManager is an abstract class in which each of its subclasses should implement the singleton design pattern
 * to ensure that multiple simultaneous reads and writes to the database are 
 * thread safe and do not corrupt database content. 
 */
public abstract class DatabaseManager extends SQLiteOpenHelper {
	
	public interface DatabaseManagerDelegate{
		public void databaseRequestDidFinishSuccessfullyWithDataAndIdentifier(ArrayList<?> list, String identifier);
		public void databaseRequestDidFailWithExceptionAndIdentifier(Exception exception, String identifier);
	}
	
	

	private Context context;

	/* Database properties */
	private String DB_PATH;
	private String DB_NAME;
	private SQLiteDatabase db;

	/* Threading Queue */
	private final int MAX_QUEUE_SIZE = 1000;
	private final int MIN_THREAD_COUNT = 0;
	private final int MAX_THREAD_COUNT = 1;
	private final int TIME_TO_LIVE = 10;

	BlockingQueue<Runnable> worksQueue = new ArrayBlockingQueue<Runnable>(MAX_QUEUE_SIZE);
	private ThreadPoolExecutor executor = new ThreadPoolExecutor(MIN_THREAD_COUNT, MAX_THREAD_COUNT, TIME_TO_LIVE,
			TimeUnit.SECONDS, worksQueue);




	protected DatabaseManager(Context context, String db_path, String db_name, int db_version)
	{
		super(context, db_name, null, db_version);
		this.context = context;
		DB_PATH = db_path;
		DB_NAME = db_name;
		this.setupDB();
	}

	public void createDataBase() throws IOException{

		boolean dbExist = checkDataBase();

		if(!dbExist){
			try {
				copyDataBase();
			} catch (IOException e) {
				throw new Error(e);
			}
		}
	}

	/**
	 * Check if the database already exist to avoid re-copying the file each time you open the application.
	 * @return true if it exists, false if it doesn't
	 */
	private boolean checkDataBase(){

		try{
			String myPath = DB_PATH + DB_NAME;
			db = SQLiteDatabase.openDatabase(myPath, null, SQLiteDatabase.OPEN_READWRITE);
			
		} catch(SQLiteException e){
			//database does't exist yet.
			System.out.println("ERROR: Database doesn't exist");
		}
		
		if(db != null){
			db.close();
		}

		return db != null ? true : false;
	}

	/**
	 * Copies your database from your local assets-folder to the just created empty database in the
	 * system folder, from where it can be accessed and handled.
	 * This is done by transfering bytestream.
	 * */
	private void copyDataBase() throws IOException{
		
		File f = new File(DB_PATH);
		if (!f.exists()) {
			f.mkdir();
		}

		//Open your local db as the input stream
		InputStream myInput = context.getAssets().open(DB_NAME);

		
		// Path to the just created empty db
		String outFileName = DB_PATH + DB_NAME;

		//Open the empty db as the output stream
		OutputStream myOutput = new FileOutputStream(outFileName);
		
		//transfer bytes from the inputfile to the outputfile
		byte[] buffer = new byte[1024];
		int length;
		while ((length = myInput.read(buffer))>0){
			myOutput.write(buffer, 0, length);
		}
		
		//Close the streams
		myOutput.flush();
		myOutput.close();
		myInput.close();
	}

	public void openDataBase() throws SQLException
	{
		String myPath = DB_PATH + DB_NAME;
		db = SQLiteDatabase.openDatabase(myPath, null, SQLiteDatabase.OPEN_READWRITE);
	}

	@Override
	public synchronized void close() {

		if(db != null) {
			db.close();
		}

		super.close();

	}

	@Override
	public void onCreate(SQLiteDatabase db) 
	{
		setupDB();
	}

	@Override
	public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) 
	{
		// Add your public helper methods to access and get content from the database.
		// You could return cursors by doing "return myDataBase.query(....)" so it'd be easy
		// to you to create adapters for your views.
		setupDB();
	}
	
	public void setupDB() {
		try {
			this.createDataBase();
		} catch (IOException ioe) {
			throw new Error(ioe);
		}
	}
	
	protected void performRunnableDatabaseRequest(Runnable_Database_Request request) 
	{		
			/* Verify that the parser responds to parse requests */ 
			if(request.canPerformParserCallback()) {			
				/* Create an asynchronous database request and add to the execution queue */
				this.queueRequest(request);
			}else {
				System.err.println("performRunnableDatabaseRequest parser does not respond");
			}
	}
	
	public void queueRequest(Runnable_Database_Request request) {
		executor.execute(request);
	}

	





	protected class Runnable_Database_Request  implements Runnable{

		private Cursor cursor;
		private String SQL = null;
		protected String[] queryParams = null;
		private DelegateHelper parserDelegate = null;
		private String parserMethodNameString = null;
		protected DatabaseManagerDelegate callbackDelegate = null;
		protected String requestIdentifier;

		public Runnable_Database_Request (String SQL, String[] queryParams, DelegateHelper parserDelegate, String parserMethodNameString, DatabaseManagerDelegate callbackDelegate, String requestIdentifier) {
			this.SQL = SQL;
			this.queryParams = queryParams;
			this.parserMethodNameString = parserMethodNameString;
			this.parserDelegate = parserDelegate;
			this.callbackDelegate = callbackDelegate;
			this.requestIdentifier = requestIdentifier;
		}

		@Override
		public void run() {		
			if(db==null || !db.isOpen()) {
				openDataBase();
			}

			/* perform the query and get a pointer to the data set */
			cursor = db.rawQuery(SQL, queryParams);
			
			/* If data set returned from database is empty, notify interface  */
			if (cursor.getCount() <= 0){
				callbackDelegate.databaseRequestDidFailWithExceptionAndIdentifier(new DatabaseManagerNoDataException(), requestIdentifier);
			}

			/* Call parser with cursor */
			this.performParserCallback();
		}
		
		public void performQuery (){
			
		}


		/* Perform Callbacks */		
		public Boolean canPerformParserCallback (){
			return (parserDelegate.respondsToMethodWithName(parserMethodNameString));
		}
		
		public void performParserCallback() {
			Object _arrParameters[] = {this};
			parserDelegate.performMethodNamedWithParameters(parserMethodNameString, _arrParameters);
		}
		
		public void performDelegateCallbackOnMainThreadWithData(final ArrayList<?> data)
		{
			Handler mainHandler = new Handler(context.getMainLooper());
			Runnable myRunnable = new Runnable(){
				public void run()
				{
					callbackDelegate.databaseRequestDidFinishSuccessfullyWithDataAndIdentifier(data, requestIdentifier);
				} 
			};
			mainHandler.post(myRunnable);
		}

		public Cursor getCursor () {
			return this.cursor;
		}

	}


	public static class DatabaseManagerNoDataException extends Exception{
		
		public DatabaseManagerNoDataException ()
		{
			super("Database query returned with 0 rows. ");
		}
		
		public DatabaseManagerNoDataException (String message) 
		{
			super(message);
		}
		
		public DatabaseManagerNoDataException (String message, Exception cause) 
		{
			super(message, cause);
		}
	}
	
	




}